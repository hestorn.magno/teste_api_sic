## Testes API

Esse projeto realiza a implementação dos métodos de validação nos endpoints da API solicitada
utilizando Java, JUnit/TestNG e RestAssured.
Também é gerado um relatório Web com os dados obtidos no teste, utilizando o Allure.
E por fim, o projeto roda em pipelines Jenkins utilizando o Jenkinsfile.

## Technology

Tecnologias utilizadas nesse projeto:

* Java version
* JUnit/TestNG
* RestAssured
* Allure



## Getting started


* Allure
>     O Allure é um framework para gerar relatórios em html utilizando os dados dos testes.
>     Para instalar, precisaremos do "scoop" antes, para isso, abra o PowerShell e digite
>     "iwr -useb get.scoop.sh | iex", com ele instalado, basta digitar "scoop install allure".

* Projeto
>     O programa é um projeto Maven feito no IntelliJ com Java.
>     Faça o clone do código, configure o Maven se necessário e digite "mvn compile", isso deverá baixar todas
>     as dependencias não instaladas ainda.

* Jenkinsfile
>Caso deseje incluir o projeto em uma pipeline Jenkins, basta importar o arquivo Jenkinsfile
na pipeline.

## How to use

Com as dependências baixadas e as configurações adicionais feitas basta 
executar os testes(arquivo TestInitialize na pasta Steps) - Ou digite
"mvn clean install" no terminal.

Após a execução, digite no powershell: allure serve C:/*CAMINHO PRO REPOSITORIO*/automacao/target/surefire-reports/
Isso irá gerar o relatório com os dados do último teste e exibirá no navegador.



## Versioning

1.0.0.0


## Authors

* **Lucas Magno S. Teichmann**: @hestorn.magno(https://gitlab.com/hestorn.magno/)





