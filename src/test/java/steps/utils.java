package steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;

public class utils {
    private static Response response;
    private static RequestSpecification httpRequest;
    private static String url;

/*
* Endpoints das APIs
* */
    @Dado("o endpoint da API simulacao")
    public void oEndpointDaAPISimulacao() {
        url = "http://localhost:8080/api/v1/simulacoes";
    }
    @Dado("o endpoint da API de restricoes")
    public void oEndpointDaAPIDeRestricoes() {
        url = "http://localhost:8080/api/v1/restricoes";
    }

    /*Asserts comuns a vários cenários
    * */
    @Entao("a API ira retornar o codigo {int}")
    public void aAPIIraRetornarOCodigo(int cod) {
        Assertions.assertEquals(response.getStatusCode(), cod);
    }

    
    /*Get e Set
    * */
    public static void setResponse(Response response) {
        utils.response = response;
    }

    public static void setHttpRequest(RequestSpecification httpRequest) {
        utils.httpRequest = httpRequest;
    }

    public static void setUrl(String url) {
        utils.url = url;
    }

    public static String getUrl() {
        return url;
    }
    public static RequestSpecification getHttpRequest() {
        return httpRequest;
    }
    public static Response getResponse() {
        return response;
    }

}
