package steps;

import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import org.junit.jupiter.api.Assertions;

public class steps_post {

    @Quando("eu enviar uma requisição POST com {string}, {string}, {string}, {string}, {string}, {string}")
    public void euEnviarUmaRequisiçãoPOSTCom(String nome, String cpf, String email, String valor, String parcelas, String seguro) {

        boolean b1=Boolean.parseBoolean(seguro);
        int parc = Integer.parseInt(parcelas);
        float val = Float.parseFloat(valor);

        String myJson = 
                "{\"nome\":\""+nome+"\","       +
                "\"cpf\": \""+cpf+"\", "        +
                "\"email\": \""+email+"\","     +
                "\"valor\": \""+val+"\","     +
                "\"parcelas\": \""+parc+"\","     +
                "\"seguro\": \""+b1+"\"}";
        
       utils.setHttpRequest(RestAssured.given().contentType("application/json").body(myJson)); 
        
       utils.setResponse(utils.getHttpRequest().post(utils.getUrl()));

    }


    @E("os dados da request contendo o {string} cadastrado")
    public void osDadosDaRequestContendoOCadastrado(String cpf) {
        Assertions.assertEquals(utils.getResponse().jsonPath().get("cpf"), cpf);
    }

    @E("a mensagem de CPF repetido")
    public void aMensagemDeCPFRepetido() {
        Assertions.assertEquals(utils.getResponse().jsonPath().get("mensagem"), "CPF já existente");
    }
}
