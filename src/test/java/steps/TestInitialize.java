package steps;

import io.cucumber.junit.CucumberOptions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;


@CucumberOptions(features="src/test/features",
        glue = "steps",
        plugin = { "pretty", "json:target/cucumber-reports/CucumberTestReport.json" } )

public class TestInitialize {

    @BeforeAll
    public static void TestSetup() {
    }

    @AfterAll
    public static void tearDown(){}
    }
