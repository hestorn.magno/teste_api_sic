#language: pt

Funcionalidade:
  Validar a exclusão de pessoas na API.

  Esquema do Cenário: Excluir uma simulacao
    Dado o endpoint da API simulacao
    Quando eu enviar uma requisição DELETE com a "<ID>" da pessoa
    Entao a API ira retornar o codigo 204
    Exemplos:
      | ID  |
      | 13  |

  Esquema do Cenário: Tentar excluir uma simulacao que não existe
    Dado o endpoint da API simulacao
    Quando eu enviar uma requisição DELETE com a "<ID>" de uma pessoa que não existe
    Entao a API ira retornar o codigo 404
    E a mensagem "Simulação não encontrada"
    Exemplos:

    |ID|
    |999|