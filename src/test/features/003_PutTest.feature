#language: pt

Funcionalidade:
  Validar alteração de uma pessoa via API.


  Esquema do Cenário: Alterar uma simulacao
    Dado o endpoint da API simulacao
    Quando eu enviar uma requisição PUT para o "<cpf>" com "<nomeAlterado>", "<emailAlterado>", "<valorAlterado>", "<parcelaAlterado>", "<seguroAlterado>"
    Entao a API ira retornar o codigo 200
    E o "<cpf>" com os dados alterado
    Exemplos:

      |nomeAlterado         |   |cpf          |   |emailAlterado    |   |valorAlterado | |parcelaAlterado |   |seguroAlterado  |
      |Lucas SeguroOn Alt   |   |11122233348  |   |teste@email.com  |   |2000.50     | |3                 |   |True     |
      |Lucas SeguroOff Alt  |   |11122233349  |   |teste@email.com  |   |1500        | |5                 |   |False    |

  Esquema do Cenário: Alterar uma simulacao - CPF sem simulação
    Dado o endpoint da API simulacao
    Quando eu enviar uma requisição PUT para o "<cpf>" com "<nomeAlterado>", "<emailAlterado>", "<valorAlterado>", "<parcelaAlterado>", "<seguroAlterado>"
    Entao a API ira retornar o codigo 404
    E a mensagem com o "<cpf>" não encontrado
    Exemplos:

      |nomeAlterado         |   |cpf |   |emailAlterado    |   |valorAlterado   | |parcelaAlterado   |   |seguroAlterado  |
      |Lucas SeguroOn Alt   |   |22  |   |teste@email.com  |   |2000.50         | |3                 |   |True    |
