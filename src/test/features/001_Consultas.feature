#language: pt

Funcionalidade:
  Validar os CPFs consultados para ver se possuem ou não restrição.

  Esquema do Cenário: Consultar CPF com restrição
    Dado o endpoint da API de restricoes
    Quando realizar uma consulta com "<CPF>"
    Entao a API ira retornar o codigo <COD>
    E e a mensagem se tiver restricao <COD>, "<CPF>"
    Exemplos:
      |CPF|         |COD  |
      |97093236014| |200  |
      |01270640100| |204  |

  Esquema do Cenário: Consultar uma simulacao pelo CPF
    Dado  o endpoint da API simulacao
    Quando realizar uma requisição GET passando o "<CPF>"
    Entao a API ira retornar o codigo 200
    E os dados da simulação cadastrada
    Exemplos:
    |CPF|
    |66414919004|

  Esquema do Cenário: Consultar pelo CPF - CPF não cadastrado
    Dado o endpoint da API simulacao
    Quando realizar uma consulta com "<CPF>"
    Entao a API ira retornar o codigo 404
    Exemplos:
      | CPF     |
      | 1231111111 |

  Cenario: Consultar todas as simulacoes cadastradas
    Dado  o endpoint da API simulacao
    Quando realizar uma consulta
    Entao a API ira retornar o codigo 200
    E a lista de simulacoes cadastradas

  Cenário: Consultar todas as simulações - Sem simulações cadastradas
    Dado  o endpoint da API simulacao
    Quando realizar uma consulta
    Entao a API ira retornar o codigo 204





